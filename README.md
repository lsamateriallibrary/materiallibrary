# Project Information and Setup

The project is setup with [Nuxt](https://nuxt.com) as base framework, uses [Nuxt Content](https://content.nuxtjs.org) and is styled with [tailwindcss](https://tailwindcss.com). For more details, please check the corresponding documentation on its respective website.

## Content Structure for Editing

- `/content/index.md` (Startpage - Markdown File)
- `/content/materialien/*.yml` (Material Documents - Yaml Files)
- `/public/materialien/*.jpg` (Images use jpg or webp)

## Local Installation

For local development, please install the LTS version of:

- Install [Node.js](https://nodejs.org/en/download) for your environment. If using Windows, make sure to install NodeJS v18 using [NVM Windows](https://github.com/coreybutler/nvm-windows).
- NodeJS should come with Yarn pre-installed. If it's missing on your system, install [Yarn](https://yarnpkg.com/getting-started/install) to build and run the local environment. For Windows the latest Build https://classic.yarnpkg.com/latest.msi

### Setup

Make sure to install all dependencies:

Switch to your project folder and run the following command.

```bash
yarn
```

### Development Server (Local)

Start the development server on http://localhost:3000

```bash
yarn dev
```

### Build

Whenever you decide to publish your project, make sure you commit to GitLab and push your changes. The project will be automatically build and published to the production server. Make sure to wait a few minutes for the build to finish.

Public URL: [https://lsamateriallibrary.gitlab.io/materiallibrary/](https://lsamateriallibrary.gitlab.io/materiallibrary/)
