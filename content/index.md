---
title: Materialbibliothek Uni.li
---

# Materialbibliothek Uni.li

Die Materialbibliothek der 'Liechtenstein School of Architecture' wurde im Rahmen eines Pro Bono Projekts von Madeleine Schwindt, Patricia Defranceschi, Simone Madlener, Aleksandra Pupovac und Sascha Steinegger erstellt. Alle Angaben sind ohne Gewähr und basieren auf den zitierten Quellen. Wir übernehmen keine Verantwortung für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte. Allfällige Anpassungen können Sascha.Steinegger@uni.li gemeldet werden.

[Alle Materialien](/materialien)
